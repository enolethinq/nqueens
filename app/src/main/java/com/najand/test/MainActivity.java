package com.najand.test;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private final static String TAG = MainActivity.class.getSimpleName();
    private int n = 9;
    private int[] c= new int[n+1];

    private TextView textView;
    private TextView solutionCounterTv;
    private String result="";
    private int resultCounter=0;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = findViewById(R.id.text_view);
        solutionCounterTv = findViewById(R.id.solution_counter_tv);
        queens(0);
        textView.setText(result);
        solutionCounterTv.setText("number of solutions: "+resultCounter);
    }
    private void queens(int i){
        Log.i(TAG, "queens: start "+ i);
        int j;
        Log.i(TAG, "queens: result--> "+promising(i));
        if (promising(i))
            if (i == n)
                report();
            else
                for (j=1; j<=n; j++){
                    c[i+1]=j;
                    queens(i+1);
                }
        Log.i("tag_tag", "queens: end");
    }

    private void report() {
        for (int value: c) {
            if (value!=0){
                result+=String.valueOf(value);
                Log.i(TAG, "report: " + value);
                result = result + ", ";
            }

        }
        result = result + "\n";
        resultCounter++;
//        textView.setText("end");
        Log.i(TAG, "report: end");
    }

    private boolean promising (int i){
        int k=1;
        while (k<i){
            Log.i("tag_tag", "promising: checking... c[i]: "+c[i]+", c[k]: "+c[k]);
            if (c[i] == c[k] || Math.abs(c[i]-c[k])==Math.abs(i-k))
                return false;
            k++;
        }
        return true;
    }
}